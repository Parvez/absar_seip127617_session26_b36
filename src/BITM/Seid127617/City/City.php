<?php
namespace App\City;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;

class City extends DB
{
    public $id;
    public $name;
    public $Location;

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION)) session_start();
    }


    public  function setData ($_postVariableData=null){
        if (array_key_exists("id", $_postVariableData)){
            $this->id = $_postVariableData["id"];
        }
        if (array_key_exists("city", $_postVariableData)){
            $this->name = $_postVariableData["city"];
        }
        if (array_key_exists("location", $_postVariableData)){
            $this->Location = $_postVariableData["location"];
        }
    }
//       public function store(){
//           $sql = "insert into book_title (title,author) values ('$this->book_title','$this->author_name')";
//           $stmt =  $this->conn->prepare($sql);
//           $stmt->execute();
//           echo "inserted";
//      }
    public function store()
    {
        $arrData = array($this->name, $this->Location);
        $sql = "Insert INTO city(city,location) VALUES (?,?)";
        $stmt = $this->conn->prepare($sql);
        $result = $stmt->execute($arrData);

        if ($result) {
            Message::setMessage("Data has been inserted successfully");
        } else {
            Message::setMessage("failed Data has not been inserted successfully");

        }

        Utility::redirect("create.php");
    }
}