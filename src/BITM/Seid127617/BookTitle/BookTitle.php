<?php
namespace App\BookTitle;

use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;
    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION)) session_start();
    }


    public  function setData ($_postVariableData=null){
            if (array_key_exists("id", $_postVariableData)){
                $this->id = $_postVariableData["id"];
            }
            if (array_key_exists("book_title", $_postVariableData)){
                $this->book_title = $_postVariableData["book_title"];
            }
            if (array_key_exists("author_name", $_postVariableData)){
                $this->author_name = $_postVariableData["author_name"];
            }
        }
//       public function store(){
//           $sql = "insert into book_title (title,author) values ('$this->book_title','$this->author_name')";
//           $stmt =  $this->conn->prepare($sql);
//           $stmt->execute();
//           echo "inserted";
//      }
    public function store(){
        $arrData = array($this->book_title,$this->author_name);
        $sql = "Insert INTO book_title(book_title, author_name) VALUES (?,?)";
        $stmt =  $this->conn->prepare($sql);
        $result= $stmt->execute($arrData);

        if($result){
            Message::setMessage("Data has been inserted successfully");
        }
        else{
            Message::setMessage("failed Data has not been inserted successfully");

        }

         Utility::redirect("create.php");
//        echo "inserted";
//        Message::setMessage();
//        Utility::redirect("create.php");
    }
    public function index($fetchMode='ASSOC'){

        $stmt = $this->conn->query('SELECT * from book_title');

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $stmt->setFetchMode(PDO::FETCH_OBJ);
        else
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $stmt->fetchAll();
        return $arrAllData;


    }// end of index();






    public function view($fetchMode='ASSOC'){

        $sql = 'SELECT * from book_title where id='.$this->id;

        $stmt = $this->conn->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $stmt->setFetchMode(PDO::FETCH_OBJ);
        else
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $stmt->fetch();
        return $arrOneData;


    }// end of view();


}