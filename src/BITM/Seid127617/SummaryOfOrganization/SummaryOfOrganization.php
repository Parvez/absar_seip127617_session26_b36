<?php
namespace App\SummaryOfOrganization;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;

class SummaryOfOrganization extends DB
{
    public $id;
    public $name;
    public $organization;

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION)) session_start();
    }


    public  function setData ($_postVariableData=null){
        if (array_key_exists("id", $_postVariableData)){
            $this->id = $_postVariableData["id"];
        }
        if (array_key_exists("name", $_postVariableData)){
            $this->name = $_postVariableData["name"];
        }
        if (array_key_exists("organization_summary", $_postVariableData)){
            $this->organization = $_postVariableData["organization_summary"];
        }
    }
//       public function store(){
//           $sql = "insert into book_title (title,author) values ('$this->book_title','$this->author_name')";
//           $stmt =  $this->conn->prepare($sql);
//           $stmt->execute();
//           echo "inserted";
//      }
    public function store()
    {
        $arrData = array($this->name, $this->organization);
        $sql = "insert into summary_of_organization (name, organization) VALUES (?,?)";
        $stmt = $this->conn->prepare($sql);
        $result = $stmt->execute($arrData);

        if ($result) {
            Message::setMessage("Data has been inserted successfully");
        } else {
            Message::setMessage("failed Data has not been inserted successfully");

        }

        Utility::redirect("create.php");
    }
}