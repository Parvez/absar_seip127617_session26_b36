<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;



class Hobbies extends DB
{

    public $id;
    public $name;
    public $hobbies;


    public function __construct()
    {
        parent::__construct();
        if(!isset ($_SESSION)) session_start();
    }
    public function setData ($postVariableData=Null){
        if (array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists("name",$postVariableData)){
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists("hobbies",$postVariableData)){
//            $this->Hobbies = implode$postVariableData['Hobbies'];
            $this->hobbies=implode(",",$postVariableData['hobbies']);
//            echo $this->hobbies;
//            die();
        }
    }
    public function store (){
        $arrData = array($this->name,$this->hobbies);
        $sql = "insert into hobbies(name,hobbies) VALUE (?, ?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrData);
        if ($result){
            Message::setMessage ("Date has been inserted successfully");
        }
        else {

            Message::setMessage ("Failed! Date has not been inserted successfully");
        }
        Utility::redirect ('create.php');
    }
}// end of BookTitle class
