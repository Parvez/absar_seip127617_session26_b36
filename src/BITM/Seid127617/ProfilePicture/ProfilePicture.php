<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;


class ProfilePicture extends DB{
    public $id="";
    public $name;
    public $profilepicture ="";

    public function __construct()
    {
        parent::__construct();
        if(!isset ($_SESSION)) session_start();
    }
    public function setData ($postVariableData=Null){
        if (array_key_exists("id",$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if (array_key_exists("name",$postVariableData)){
            $this->name = $postVariableData['name'];
        }


        $this->profilepicture = time().$_FILES['profile_picture']['name'];
        $temporary_location = $_FILES['profile_picture']['tmp_name'];

        move_uploaded_file($temporary_location,'../../../picture/'.$this->profilepicture);


    }
    public function store (){
        $arrData = array($this->name,$this->profilepicture);

        $sql = "insert into profilepicture (name,Profilepicture) VALUE (?, ?)";
        $STH= $this->conn->prepare($sql);
        $result= $STH->execute($arrData);
        if ($result){
            Message::setMessage ("Date has been inserted successfully");
        }
        else {

            Message::setMessage ("Failed! Date has not been inserted successfully");
        }
        Utility::redirect ('create.php');
    }
}
